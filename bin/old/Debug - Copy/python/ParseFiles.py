import copy
import re

debug = False

def debug_print(_str):
    if debug:
        print(_str)
 
class GMLStruct:
    def __init__(self, _name, _call, _description, _params, _methods, _returnValue):
        self.name = _name
        self.call = _call
        self.description = _description
        self.params = _params
        self.methods = _methods
        self.returnValue = _returnValue
    def __str__(self):
        return "Struct: "+self.name+"\n Call: "+self.call+"\n Description: "+self.description+"\n Params:\n"+" ".join([str(x) for x in self.params])+"\n Methods:\n"+" ".join([str(x) for x in self.methods])+"\n Struct return value: "+str(self.returnValue)
   
class GMLFunction:
    def __init__(self, _name, _call, _description, _params, _returnValue):
        self.name = _name
        self.call = _call
        self.description = _description
        self.params = _params
        self.returnValue = _returnValue
    def __str__(self):
        return "Function: "+self.name+"\n Call: "+self.call+"\n Description: "+self.description+"\n Params:\n"+" ".join([str(x) for x in self.params])+"\n Function return value: "+str(self.returnValue)+"\n"
       
class GMLParam:
    def __init__(self, _name, _type, _description, _optional):
        self.name = _name
        self.type = _type
        self.description = _description
        self.optional = _optional
    def __str__(self):
        return "Param: "+self.name+"\n Type: "+self.type+"\n Description: "+self.description+"\n Optional: "+str(self.optional)+"\n"
       
class GMLReturnValue:
    def __init__(self, _type, _description):
        self.type = _type
        self.description = _description
    def __str__(self):
        return "Return value type: "+self.type+"\n Description: "+self.description
 
def append_function():
    global functions
    global obj_function
    if obj_function != None:        
        obj_copy = copy.copy(obj_function)
        debug_print("I should be adding "+str(type(obj_copy))+" "+obj_copy.name)
        if isinstance(obj_copy, GMLFunction):   
            functions.append(obj_copy)                   
            obj_function = None
 
def append_struct():
    global structs
    global obj_struct
    if obj_struct != None:
        obj_copy = copy.copy(obj_struct)
        debug_print("I should be adding "+str(type(obj_copy))+" "+obj_copy.name)
        if isinstance(obj_copy, GMLStruct):   
            structs.append(obj_copy)                   
            obj_struct = None
            
 
def append_method():
    global structs
    global functions
    global obj_struct
    global obj_function
    global obj_method
    global struct_active
    global function_active
   
    if obj_method != None:
        obj_copy = copy.copy(obj_method)
        debug_print("I should be adding "+str(type(obj_copy))+" "+obj_copy.name)
        if struct_active:
            obj_struct.methods.append(obj_copy)           
            obj_method = None
 
           
def parse_obj_header(_target, _line): # Can be function or struct
    result = re.search(r'@(function|func|struct|constructor)\s+([A-Za-z_]+)', _line)
    _target.name = result.group(2)
    result = re.search(r'@(function|func|struct|constructor)\s+(.*)', _line)
    _target.call = result.group(2)
 
def parse_obj_header_method(_target, _line): # Can a method
    result = re.search(r'@(method)\s+([A-Za-z_]+)', _line)
    _target.name = result.group(2)
    result = re.search(r'@(method)\s+(.*)', _line)
    _target.call = result.group(2)
 
   
def append_obj_description(_line):
    _target = None        
    if method_active:
        _target = obj_method
        debug_print("  desc target: method")
    elif struct_active:
        _target = obj_struct
        debug_print("  desc target: struct")
    elif function_active:
        _target = obj_function
        debug_print("  desc target: function")
    
    global obj_return
    global obj_param
    if return_active:
        _target = obj_return
    elif param_active:
        _target = obj_param
    
    if _target != None:
        result = re.search(r'((@description))\s*(.*)', _line)
        if result != None:            
            debug_print("  confirmed description insert")            
            _target.description += result.group(3)
        else:
            #_target.description += _line
            result2 = re.search(r'\s*\/\/\/\t*(.*)', _line)
            if result2 != None:
                debug_print("  confirmed description append: "+result2.group(1))     
                _target.description += " "+result2.group(1)
 
def parse_obj_param(_line):
    _target = None
    if method_active:
       _target = obj_method
       debug_print("  param target: method")
    elif struct_active:
       _target = obj_struct
       debug_print("  param target: struct")
    elif function_active:
       _target = obj_function
       debug_print("  param target: function")
    
    global obj_param
    obj_param = GMLParam("", "Any", "", False)
    result = re.search(r'@(param|parameter|arg|argument)\s+({[A-Za-z._\*]+})\s+([\[A-Za-z_\]]+)\s*(.*)', _line)
    if result != None and _target != None:
        obj_param.name = result.group(3).removeprefix("[").removesuffix("]")
        obj_param.type = result.group(2).removeprefix("{").removesuffix("}")
        obj_param.description = result.group(4)
        obj_param.optional = result.group(3).find("[") >= 0 and result.group(3).find("]") >= 0
        _target.params.append(obj_param)
        debug_print("  confirmed param append with type")
    else:
        result = re.search(r'@(param|parameter|arg|argument)\s+([\[A-Za-z_\]]+)\s*(.*)', _line)
        if result != None and _target != None:
            obj_param.name = result.group(2).removeprefix("[").removesuffix("]")           
            obj_param.description = result.group(3)
            obj_param.optional = result.group(2).find("[") >= 0 and result.group(2).find("]") >= 0
            _target.params.append(obj_param)
            debug_print("  confirmed param append without type")
    
        
def parse_obj_return(_line):
    _target = None
    if method_active:
        _target = obj_method
        debug_print("  return target: method")
    elif struct_active:
        _target = obj_struct
        debug_print("  return target: struct")
    elif function_active:
        _target = obj_function
        debug_print("  return target: function")
    
    global obj_return
    obj_return = GMLReturnValue("Any", "")
    result = re.search(r'@(returns|return)\s+({[A-Za-z_\*]+})\s*(.*)', _line)
    if result != None and _target != None:
        obj_return.type = result.group(2).removeprefix("{").removesuffix("}")
        obj_return.description = result.group(3)
        _target.returnValue = obj_return
        debug_print("  confirmed return append with type")
    else:
        result = re.search(r'@(returns|return)\s*(.*)', _line)
        if result != None and _target != None:
            obj_return.description = result.group(2)
            _target.returnValue = obj_return
            debug_print("  confirmed return append without type")
 
# ------------------------------
# Variables
# ------------------------------
 
path = "C:\\Users\\biyec\\OneDrive\\Escritorio\\"
 
with open(path+"files.txt", 'r', encoding='utf-8') as listfile:
    for filename in listfile:
        print("##### PROCESSING: "+filename.strip("\r\n")+" #####")
        # Reinitialize variables       
        structs = []
        functions = []
       
        obj_struct = None
        obj_function = None
        obj_method = None
        obj_param = None
        obj_return = None
       
        struct_active = False
        method_active = False
        function_active = False
        param_active = False
        return_active = False
        description_active = False           
                
        block_comment_active = False
       
        with open(path+filename.strip("\r\n"), 'r', encoding='utf-8') as file:            
            for line in file:     
                      
                result_blockstart = re.search(r'\s*\/\*\*', line)
                result_blockend = re.search(r'\s*\*\/', line)                                
                result_normal = re.search(r'\s*\/\/\/', line)
                #if line.startswith("/**"):
                if result_blockstart != None:
                    block_comment_active = True
                #elif line.startswith("*/") and block_comment_active:
                elif result_blockend != None and block_comment_active:
                    block_comment_active = False
                #elif line.startswith("///") or block_comment_active:
                elif result_normal != None or block_comment_active:
                    if line.find("@struct") >= 0 or line.find("@constructor") >= 0:                       
                        debug_print(" Appending active struct")
                        # Append current stuff
                        if method_active:
                            debug_print(" Appending active method to the previous struct")
                            append_method()
                        append_struct()
                        # Create new struct
                        debug_print(" Creating new struct")
                        obj_struct = GMLStruct("", "", "", [], [], "")
                        struct_active = True
                        function_active = False
                        method_active = False
                        param_active = False
                        return_active = False
                        description_active = False
                       
                        parse_obj_header(obj_struct, line)
                    elif line.find("@function") >= 0 or line.find("@func") >= 0:
                        debug_print(" Appending active function")
                        # Append current stuff
                        append_function()
                        debug_print(" Creating new function")
                        # Create new function
                        obj_function = GMLFunction("", "", "", [], "")
                        struct_active = False
                        function_active = True
                        method_active = False
                        param_active = False
                        return_active = False
                        description_active = False
                        
                        parse_obj_header(obj_function, line)
                    elif line.find("@method") >= 0:
                        debug_print(" Appending active method, if available, to corresponding struct")
                        # Append current stuff
                        append_method()
                        # Create new struct
                        debug_print(" Creating new method")
                        obj_method = GMLFunction("", "", "", [], "") 
                        method_active = True
                        function_active = False
                        param_active = False
                        return_active = False
                        description_active = False
                       
                        parse_obj_header_method(obj_method, line)
                    elif line.find("@description") >= 0 or line.find("@desc") >= 0:                       
                            debug_print(" Appending description")
                            description_active = True                            
                            append_obj_description(line)
                    elif line.find("@param") >= 0 or line.find("@parameter") >= 0 or line.find("@arg") >= 0 or line.find("@argument") >= 0:
                       
                            debug_print(" Appending param")
                            param_active = True
                            description_active = True
                            parse_obj_param(line)
                    elif line.find("@return") >= 0 or line.find("@returns") >= 0:
                       
                            debug_print(" Appending return")
                            return_active = True
                            description_active = True
                            parse_obj_return(line)
                   
                    elif description_active:
                       
                            debug_print(" Appending additional line for description")
                            append_obj_description(line)                   
                    else:
                        line = "DISCARDED: "+line
                    debug_print(line)
                
        # Append remaining
        if struct_active:
            if method_active:
                debug_print(" Appending active method at end of file")
                append_method()
            debug_print(" Appending active struct at end of file")
            append_struct()
        if function_active:
            debug_print(" Appending active function at end of file")
            append_function()

        print()
        print("##### RESULTS FOR FILE "+filename.strip("\r\n")+" #####")
        print()
                  
        for item in structs:
            print(item)
        for item in functions:
            print(item)
            
