
<!-- JS -->
	
	<script src="js/vendor/modernizr-3.11.2.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/main.js"></script>
	<script>
		document.addEventListener('DOMContentLoaded', (event) => {
			$('#toctable').tablesort();
			$('thead th#idcolumn').data(
				'sortBy', 
				function(th, td, tablesort) {
					return parseFloat(td.text());
				}
			);
		});
	</script>
</body>

</html>
