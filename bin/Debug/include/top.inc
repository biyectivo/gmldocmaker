<!doctype html>
<html class="no-js" lang="en">
<!-- Generated with GMLDocMaker -->
<head>
	<meta charset="utf-8">
	<title>$TITLE$</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta property="og:title" content="">
	<meta property="og:type" content="">
	<meta property="og:url" content="">
	<meta property="og:image" content="">

	<link rel="manifest" href="site.webmanifest">
	<link rel="apple-touch-icon" href="icon.png">
	<link rel="icon" href="icon.png">
	<!-- favicon.ico in the root directory -->

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">

	<meta name="theme-color" content="#fafafa">
	
	<!-- jQuery before Fomantic -->
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.9.0/dist/semantic.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.9.0/dist/semantic.min.js"></script>
	<script src="js/tablesort.js"></script>
</head>

<body>

	<!-- Content -->
