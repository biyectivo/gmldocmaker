﻿Imports System.Linq.Expressions

Public Class MainForm

    Public Const version As String = "0.3"

    Public Sub AddFiles(directory As String)
        Dim dirInfo = New System.IO.DirectoryInfo(directory)
        Dim folderInfoArray = dirInfo.GetDirectories()
        Dim fileInfoArray = dirInfo.GetFiles("*.gml")
        Dim fileInfo As System.IO.FileInfo
        Dim dir As System.IO.DirectoryInfo

        ' Proecss GML files
        For Each fileInfo In fileInfoArray
            CheckedListBox1.Items.Add(fileInfo.FullName)
            CheckedListBox1.SetItemChecked(CheckedListBox1.Items.Count - 1, True)
        Next

        ' Process subfolders
        For Each dir In folderInfoArray
            AddFiles(dir.FullName)
        Next
    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "GMLDocMaker v" + version + " by manta ray"

        ' Load info
        'MsgBox(My.Computer.FileSystem.CurrentDirectory)
        If My.Computer.FileSystem.FileExists("settings.dat") Then
            Try
                FileOpen(1, "settings.dat", OpenMode.Input)
                Dim idir As String = ""
                Dim odir As String = ""
                Dim pack As String = ""

                Input(1, idir)
                Input(1, odir)
                Input(1, pack)

                Me.pageTitleTextbox.Text = pack

                If System.IO.Directory.Exists(idir) Then
                    CheckedListBox1.Items.Clear()
                    Me.inDirectoryTextbox.Text = idir
                    AddFiles(Me.inDirectoryTextbox.Text)
                End If

                If System.IO.Directory.Exists(odir) Then
                    Me.outDirectoryTextbox.Text = odir
                End If


                FileClose(1)
            Catch ex As Exception
                MsgBox(ex.Message + vbCrLf + ex.StackTrace)
            End Try

        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles inDirectoryBrowseButton.Click

        If My.Computer.FileSystem.DirectoryExists(inDirectoryTextbox.Text) Then
            inDirectoryBrowserDialog.SelectedPath = inDirectoryTextbox.Text
        End If
        Dim dialogResult = inDirectoryBrowserDialog.ShowDialog()

        If dialogResult = DialogResult.OK Then
            inDirectoryTextbox.Text = inDirectoryBrowserDialog.SelectedPath
            CheckedListBox1.Items.Clear()

            AddFiles(inDirectoryTextbox.Text)
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles outDirectoryBrowseButton.Click
        Dim dialogResult = outDirectoryBrowserDialog.ShowDialog()

        If dialogResult = DialogResult.OK Then
            outDirectoryTextbox.Text = outDirectoryBrowserDialog.SelectedPath
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles checkAllButton.Click
        Dim i As Integer
        Dim n As Integer = CheckedListBox1.Items.Count
        For i = 0 To n - 1
            CheckedListBox1.SetItemChecked(i, True)
        Next i
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles uncheckAllButton.Click
        Dim i As Integer
        Dim n As Integer = CheckedListBox1.Items.Count
        For i = 0 To n - 1
            CheckedListBox1.SetItemChecked(i, False)
        Next i
    End Sub


    Private Sub inDirectoryTextbox_KeyDown(sender As Object, e As KeyEventArgs) Handles inDirectoryTextbox.KeyDown
        If e.Modifiers = Keys.Control AndAlso e.KeyCode = Keys.V Then
            Dim dir As String
            dir = Clipboard.GetText

            If My.Computer.FileSystem.DirectoryExists(dir) Then
                CheckedListBox1.Items.Clear()
                inDirectoryBrowserDialog.SelectedPath = dir
                AddFiles(dir)
            Else
                Debug.Print(dir + " Does not exist")
            End If
        End If
        e.Handled = True
    End Sub

    Private Sub makeDocumentationButton_Click(sender As Object, e As EventArgs) Handles makeDocumentationButton.Click
        If inDirectoryTextbox.Text = "" Then
            MsgBox("Please select an input directory for your GML scripts.")
        ElseIf CheckedListBox1.Items.Count = 0 Then
            MsgBox("Please select an input directory that contains at least one GML script.")
        ElseIf CheckedListBox1.CheckedItems.Count = 0 Then
            MsgBox("Please select at least one of your GML scripts.")
        ElseIf outDirectoryTextbox.Text = "" Then
            MsgBox("Please select an output directory for your HTML documentation.")
        ElseIf pageTitleTextbox.Text = "" Then
            MsgBox("The HTML project name cannot be blank.")
        Else
            ' Update settings
            UpdateSettingsFile()

            Dim frm As New ProcessingForm
            Me.Hide()
            frm.Show()
            frm.AssignForm(Me)
            frm.htmlBackgroundWorker.RunWorkerAsync()
        End If
    End Sub


    Private Sub UpdateSettingsFile()
        Try
            FileOpen(1, "settings.dat", OpenMode.Output)
            PrintLine(1, inDirectoryTextbox.Text)
            PrintLine(1, outDirectoryTextbox.Text)
            PrintLine(1, pageTitleTextbox.Text)
            FileClose(1)
        Catch ex As Exception
            MsgBox(ex.Message + vbCrLf + ex.StackTrace)
        End Try

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click

        Dim dialogResult = selectImageDialog.ShowDialog()

        If dialogResult = DialogResult.OK And My.Computer.FileSystem.FileExists(selectImageDialog.FileName) Then
            imageText.Text = selectImageDialog.FileName
        End If
    End Sub
End Class
