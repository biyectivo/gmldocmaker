
import sys
import shutil
import glob
import copy
import os
from os.path import exists
import re
import time
from datetime import datetime

start_time = datetime.now()
total_time = time.perf_counter()

print("GMLDocMaker CLI v0.12")
print("Started at "+start_time.strftime("%Y%m%d %H:%M:%S"))
print()

debug = False

# Parameters
num_args = len(sys.argv)
if num_args <= 2:
    print("ERROR: No file list and/or output directory was provided. Exiting.")
    sys.exit(1)
else:
    filename_list = sys.argv[1]    
    output_directory = sys.argv[2]
    
    if num_args >= 4:
        package_name = sys.argv[3]
    else:
        package_name = "GML Project"

    if num_args >= 5:
        package_version = sys.argv[4]
    else:
        package_version = ""

    if num_args >= 6:
        debug = sys.argv[5]

print("GML file list: "+filename_list)
print("Output directory for generated HTML: "+output_directory)
print("Package name: "+package_name)
if package_version != "":
    print("Package version: "+package_version)
print()

def debug_print(_str=""):
    if debug:
        print(_str)
 
class GMLStruct:
    def __init__(self, _name, _call, _extends, _description, _params, _methods, _returnValue):
        self.name = _name
        self.call = _call
        self.extends = _extends
        self.description = _description
        self.params = _params
        self.methods = _methods
        self.returnValue = _returnValue
    def __str__(self):
        return "Struct: "+self.name+"\n Call: "+self.call+"\n Description: "+self.description+"\n Params:\n"+" ".join([str(x) for x in self.params])+"\n Methods:\n"+" ".join([str(x) for x in self.methods])+"\n Struct return value: "+str(self.returnValue)
    def __lt__(self, other):
        return self.name < other.name
       
class GMLFunction:
    def __init__(self, _name, _call, _description, _params, _returnValue):
        self.name = _name
        self.call = _call
        self.description = _description
        self.params = _params
        self.returnValue = _returnValue
    def __str__(self):
        return "Function: "+self.name+"\n Call: "+self.call+"\n Description: "+self.description+"\n Params:\n"+" ".join([str(x) for x in self.params])+"\n Function return value: "+str(self.returnValue)+"\n"
    def __lt__(self, other):
        return self.name < other.name
       
class GMLParam:
    def __init__(self, _name, _type, _description, _optional):
        self.name = _name
        self.type = _type
        self.description = _description
        self.optional = _optional
    def __str__(self):
        return "Param: "+self.name+"\n Type: "+self.type+"\n Description: "+self.description+"\n Optional: "+str(self.optional)+"\n"
       
class GMLReturnValue:
    def __init__(self, _type, _description):
        self.type = _type
        self.description = _description
    def __str__(self):
        return "Return value type: "+self.type+"\n Description: "+self.description
 
def append_function():
    global functions
    global obj_function
    if obj_function != None:        
        obj_copy = copy.copy(obj_function)
        debug_print("I should be adding "+str(type(obj_copy))+" "+obj_copy.name)
        if isinstance(obj_copy, GMLFunction):   
            debug_print(" actually appended function")
            functions.append(obj_copy)                   
            obj_function = None
 
def append_struct():
    global structs
    global obj_struct
    if obj_struct != None:
        obj_copy = copy.copy(obj_struct)
        debug_print("I should be adding "+str(type(obj_copy))+" "+obj_copy.name)
        if isinstance(obj_copy, GMLStruct):   
            debug_print(" actually appended struct")
            structs.append(obj_copy)                   
            obj_struct = None
            
 
def append_method():
    global structs
    global functions
    global obj_struct
    global obj_function
    global obj_method
    global struct_active
    global function_active
   
    if obj_method != None:
        obj_copy = copy.copy(obj_method)
        debug_print("I should be adding "+str(type(obj_copy))+" "+obj_copy.name)
        if struct_active:
            debug_print(" actually appended method")
            obj_struct.methods.append(obj_copy)           
            obj_method = None
 
           
def parse_obj_header(_target, _line): # Can be function or struct
    result = re.search(r'@(function|func|struct|constructor)\s+([A-Za-z0-9_.]+)', _line)
    _target.name = result.group(2)
    result = re.search(r'@(function|func|struct|constructor)\s+(.*)', _line)
    _target.call = result.group(2)
 
def parse_obj_header_method(_target, _line): # Can be a method
    result = re.search(r'@(method)\s+([A-Za-z0-9_.]+)', _line)
    _target.name = result.group(2)
    result = re.search(r'@(method)\s+(.*)', _line)
    _target.call = result.group(2)
 
   
def append_obj_description(_line):
    _target = None        
    if method_active:
        _target = obj_method
        debug_print("  desc target: method")
    elif struct_active:
        _target = obj_struct
        debug_print("  desc target: struct")
    elif function_active:
        _target = obj_function
        debug_print("  desc target: function")
    
    global obj_return
    global obj_param
    if return_active:
        _target = obj_return
    elif param_active:
        _target = obj_param
    
    global block_comment_active
    
    if _target != None:
        result = re.search(r'((@description|@desc))\s*(.*)', _line)
        if result != None:            
            debug_print("  confirmed description insert")            
            _target.description += result.group(3)
        else:
            #_target.description += _line
            result2 = re.search(r'\s*\/\/\/\t*(.*)', _line)
            if result2 != None:
                debug_print("  confirmed description append: "+result2.group(1))     
                _target.description += " "+result2.group(1)
            else:
                result3 = re.search(r'\s*\*\t*(.*)', _line)
                if result3 != None:
                    debug_print("  confirmed description append of block comment: "+result3.group(1))
                    _target.description += " "+result3.group(1)
 
def parse_obj_param(_line):
    _target = None
    if method_active:
       _target = obj_method
       debug_print("  param target: method")
    elif struct_active:
       _target = obj_struct
       debug_print("  param target: struct")
    elif function_active:
       _target = obj_function
       debug_print("  param target: function")
    
    global obj_param
    obj_param = GMLParam("", "Any", "", False)
    result = re.search(r'@(param|parameter|arg|argument)\s+({[A-Za-z0-9._<>\*]+})\s+([\[A-Za-z0-9_\]]+)\s*(.*)', _line)
    if result != None and _target != None:
        obj_param.name = result.group(3).removeprefix("[").removesuffix("]")
        obj_param.type = result.group(2).removeprefix("{").removesuffix("}")
        obj_param.description = result.group(4)
        obj_param.optional = result.group(3).find("[") >= 0 and result.group(3).find("]") >= 0
        _target.params.append(obj_param)
        debug_print("  confirmed param append with type")
    else:
        result = re.search(r'@(param|parameter|arg|argument)\s+([\[A-Za-z0-9_\]]+)\s*(.*)', _line)
        if result != None and _target != None:
            obj_param.name = result.group(2).removeprefix("[").removesuffix("]")           
            obj_param.description = result.group(3)
            obj_param.optional = result.group(2).find("[") >= 0 and result.group(2).find("]") >= 0
            _target.params.append(obj_param)
            debug_print("  confirmed param append without type")

def append_extends_data(_line):
    global obj_struct
    result = re.search(r"@(extends|augments)\s+([A-Za-z0-9_.]+)", _line)
    if result != None:
        obj_struct.extends = result.group(2)
        
def parse_obj_return(_line):
    _target = None
    if method_active:
        _target = obj_method
        debug_print("  return target: method")
    elif struct_active:
        _target = obj_struct
        debug_print("  return target: struct")
    elif function_active:
        _target = obj_function
        debug_print("  return target: function")
    
    global obj_return
    obj_return = GMLReturnValue("Any", "")
    result = re.search(r'@(returns|return)\s+({[A-Za-z0-9._<>\*]+})\s*(.*)', _line)
    if result != None and _target != None:
        obj_return.type = result.group(2).removeprefix("{").removesuffix("}")
        obj_return.description = result.group(3)
        _target.returnValue = obj_return
        debug_print("  confirmed return append with type")
    else:
        result = re.search(r'@(returns|return)\s*(.*)', _line)
        if result != None and _target != None:
            obj_return.description = result.group(2)
            _target.returnValue = obj_return
            debug_print("  confirmed return append without type")

def html(_file, _tag, _level):
    _tabs = "".join(["\t" for x in range(0, _level)])
    _file.write(_tabs + _tag + "\n")

def markdown_to_html(_text):
    return re.sub(r'\*(.*?)\*', r'<em>\1</em>', 
                              re.sub(r'\*{2}(.*?)\*{2}', r'<strong>\1</strong>', 
                                     re.sub(r'\*{3}(.*?)\*{3}', r'<em><strong>\1</strong></em>', 
                                            re.sub(r'\[(.+)\]\((.*)\)', r"<a href='\2'>\1</a>", 
                                                   re.sub(r'`(.*?)`', r'<code>\1</code>', _text, flags=re.MULTILINE), flags=re.MULTILINE), flags=re.MULTILINE), flags=re.MULTILINE), flags=re.MULTILINE)

def copy_files(src, dest):
    if exists(dest):
        for file in glob.glob(src+"\*.*"):
            shutil.copy(file, dest)
    else:
        shutil.copytree(src, dest)
        

# ------------------------------
# Process
# ------------------------------

print("Python dir: "+os.getcwd())
if os.path.exists("..\\include"):    
    print("Copying include header/footer...")
    #shutil.copy("..\\include\\top.inc", output_directory)
    #shutil.copy("..\\include\\bottom.inc", output_directory)
    copy_files("..\include", output_directory)
    print("Copying CSS, JS and IMG files...")
    copy_files("..\css", output_directory+"\css")
    copy_files("..\js", output_directory+"\js")
    copy_files("..\img", output_directory+"\img")
else:
    print("Copying include header/footer...")
    #shutil.copy("include\\top.inc", output_directory)
    #shutil.copy("include\\bottom.inc", output_directory)
    copy_files("include", output_directory)
    print("Copying CSS, JS and IMG files...")
    copy_files("css", output_directory+"\css")
    copy_files("js", output_directory+"\js")
    copy_files("img", output_directory+"\img")

print()

file_list = []

with open(filename_list, 'r', encoding='utf-8') as listfile:
    for filename in listfile:
        file_list.append(filename)        

total_structs_documented = 0
total_functions_documented = 0

with open(filename_list, 'r', encoding='utf-8') as listfile:
    for filename in listfile:        
        print("> Processing: "+filename.strip("\r\n"))
        # Reinitialize variables       
        structs = []
        functions = []
       
        obj_struct = None
        obj_function = None
        obj_method = None
        obj_param = None
        obj_return = None
       
        struct_active = False
        method_active = False
        function_active = False
        param_active = False
        return_active = False
        description_active = False           
                
        block_comment_active = False
       
        with open(filename.strip("\r\n"), 'r', encoding='utf-8') as file:            
            for line in file:                     
                result_blockstart = re.search(r'\s*\/\*\*', line)
                result_blockend = re.search(r'\s*\*\/', line)                                
                result_normal = re.search(r'\s*\/\/\/', line)
                if result_blockstart != None:
                    block_comment_active = True                
                if result_blockend != None and block_comment_active:
                    block_comment_active = False                
                if result_normal != None or block_comment_active:
                    debug_print(obj_struct is None)
                    
                    if line.find("@struct") >= 0 or line.find("@constructor") >= 0:                       
                        
                        # Append current stuff
                        append_function()                        
                        if method_active:
                            debug_print(" Appending active method to the previous struct")
                            append_method()
                        debug_print(" Appending active struct")
                        append_struct()
                        # Create new struct
                        debug_print(" Creating new struct")
                        obj_struct = GMLStruct("", "", "", "", [], [], "")
                        struct_active = True
                        function_active = False
                        method_active = False
                        param_active = False
                        return_active = False
                        description_active = False
                       
                        parse_obj_header(obj_struct, line)
                    elif (line.find("@augments") >= 0 or line.find("@extends") >= 0) and struct_active:
                        debug_print(" Appending parent (inherit) data")
                        append_extends_data(line)
                    elif line.find("@function") >= 0 or line.find("@func") >= 0:
                        debug_print(" Appending active function")
                        # Append current stuff
                        append_function()
                        if method_active:
                            debug_print(" Appending active method to the previous struct")
                            append_method()
                        append_struct()
                        debug_print(" Creating new function")
                        # Create new function
                        obj_function = GMLFunction("", "", "", [], "")
                        struct_active = False
                        function_active = True
                        method_active = False
                        param_active = False
                        return_active = False
                        description_active = False
                        
                        parse_obj_header(obj_function, line)
                    elif line.find("@method") >= 0:
                        debug_print(" Appending active method, if available, to corresponding struct")
                        # Append current stuff                        
                        if method_active:
                            debug_print(" Appending active method to the previous struct")
                            append_method()                        
                        # Create new struct
                        debug_print(" Creating new method")
                        obj_method = GMLFunction("", "", "", [], "") 
                        method_active = True
                        function_active = False
                        param_active = False
                        return_active = False
                        description_active = False
                       
                        parse_obj_header_method(obj_method, line)
                    elif line.find("@description") >= 0 or line.find("@desc") >= 0:                       
                            debug_print(" Appending description")
                            description_active = True                            
                            append_obj_description(line)
                    elif line.find("@param") >= 0 or line.find("@parameter") >= 0 or line.find("@arg") >= 0 or line.find("@argument") >= 0:
                       
                            debug_print(" Appending param")
                            param_active = True
                            description_active = True
                            parse_obj_param(line)
                    elif line.find("@return") >= 0 or line.find("@returns") >= 0:
                       
                            debug_print(" Appending return")
                            return_active = True
                            description_active = True
                            parse_obj_return(line)
                   
                    elif description_active:
                       
                            debug_print(" Appending additional line for description")
                            append_obj_description(line)                   
                    else:
                        line = "DISCARDED: "+line
                    debug_print(line)
                
        # Append remaining
        if struct_active:
            if method_active:
                debug_print(" Appending active method at end of file")
                append_method()
            debug_print(" Appending active struct at end of file")
            append_struct()
        if function_active:
            debug_print(" Appending active function at end of file")
            append_function()

        if len(structs) + len(functions) > 0:
            print(f"  ...found and parsed {str(len(structs))} structs and {str(len(functions))} functions")
            total_structs_documented += len(structs)
            total_functions_documented += len(functions)
        
        debug_print()
        debug_print("##### RESULTS FOR FILE "+filename.strip("\r\n")+" #####")
        debug_print()
                          
        for item in structs:
            item.methods.sort()
            debug_print(item)
        for item in functions:
            debug_print(item)
                
        structs.sort()
        functions.sort()
        
        
        
        # Output to HTML        
        # Only output if there is at least one function or struct
        
        if len(structs) + len(functions) > 0:
            result = re.search(r'([^\\]+$)', filename.strip("\r\n"))
            filename_without_path = result.group(1)
            
            with open(output_directory+'\\doc_'+filename_without_path+'.html', 'w', encoding = 'utf-8') as outfile:
                # Head
                with open(output_directory+'\\top.inc', 'r', encoding = 'utf-8') as topfile:
                    for l in topfile:
                        outfile.write(l.replace("$TITLE$", package_name))
                
                # Menu
                level = 1
                html(outfile, "<div class='ui vertical inverted wide sidebar menu visible' id='toc'>", level)
                
                html(outfile, "<div class='item'>", level+1)
                if exists(output_directory+"\img\image.png"):
                    html(outfile, "<img class='ui small image' src='img/image.png' alt='"+package_name+"'>", level+2)
                
                if package_version != "":
                    html(outfile, "<div class='header'><span class='ui big center aligned text'>"+package_name+"</span></div><br><span class='ui small center aligned text'>VERSION "+package_version+"</span>", level+2)
                else:
                    html(outfile, "<div class='header'><span class='ui big center aligned text'>"+package_name+"</span></div>", level+2)
                    
                html(outfile, "</div>", level+1)
                           
                # files
                html(outfile, "<div id='GMLFilesDropdown' class='ui small selection dropdown center'>", level+1)
                html(outfile, "<input type='hidden' name='GML Files'>", level+2)
                html(outfile, "<i class='dropdown icon'></i>", level+2)
                html(outfile, "<div class='default text'>GML Files</div>", level+2)
                html(outfile, "<div class='scrollhint menu'>", level+2)
    
                file_list.sort()
                for f in file_list:
                    result = re.search(r'([^\\]+$)', f.strip("\n"))
                    dropdown_filename_without_path = result.group(1)                
                    html(outfile, "<div class='item' data-value='doc_"+dropdown_filename_without_path+".html'>"+dropdown_filename_without_path+"</div>", level+3)
     
                html(outfile, "</div>", level+2)
                html(outfile, "</div>", level+1)
    
    
                # structs
                html(outfile, "<div class='item'>", level+1)
                html(outfile, "<div class='header'>Structs</div>", level+2)
                html(outfile, "<div class='menu'>", level+2)
                
                for item in structs:                
                    html(outfile, "<a class='item' href='#"+item.name+"'>"+item.name+"</a>", level+3)
                    html(outfile, "<div class='menu'>", level+3)
                    for met in item.methods:
                        #html(outfile, "<a class='item'  href='#"+item.name+"."+met.name+"'><span class='ui blue text'>."+met.name+"</span></a>", level+4)
                        html(outfile, "<a class='item' href='#"+item.name+"."+met.name+"'>&nbsp;&nbsp;."+met.name+"</a>", level+4)
                    html(outfile, "</div>", level+3)
                
                html(outfile, "</div>", level+2)
                html(outfile, "</div>", level+1)
                
                # functions
                html(outfile, "<div class='item'>", level+1)
                html(outfile, "<div class='header'>Functions</div>", level+2)
                html(outfile, "<div class='menu'>", level+2)
                
                for item in functions:                
                    html(outfile, "<a class='item' href='#"+item.name+"'>"+item.name+"</a>", level+3)
                
                html(outfile, "</div>", level+2)
                html(outfile, "</div>", level+1)
                
                # Documented
                html(outfile, "<div class='ui center aligned message small inverted'>", level+1)
                html(outfile, "<div class='content'>", level+2)
                html(outfile, "<div class='header'>", level+3)
                html(outfile, "Documented automagically using <a href='https://manta-ray.itch.io/gmldocmaker'>GMLDocMaker</a>", level+4)
                html(outfile, "</div>", level+3)
                html(outfile, "</div>", level+2)
                html(outfile, "</div>", level+1)
                
                html(outfile, "</div>", level)
                
                # Content
                html(outfile, "<div class='main ui container pusher'>", level)
                html(outfile, "<br>", level)
                html(outfile, "<h1 class='ui block center aligned header'>"+package_name+"</h1>", level+1)
                html(outfile, "<div class='ui segment center aligned'>"+filename.strip("\n")+"</div>", level+1)
                            
                # Structs
                for item in structs:
                    html(outfile, "<div id='"+item.name+"' class='ui raised segment'>", level)
                    html(outfile, "<div class='ui blue ribbon label'>STRUCT</div>", level+1)
                    html(outfile, "<span class='ui large text'>"+item.name+"</span>", level+1)
                    html(outfile, "</div>", level)                
                    
                    if item.extends != "":
                        html(outfile, "<div class='ui'>", level)
                        html(outfile, "<div class='ui tag label'>Parent</div>&nbsp;&nbsp;", level+1)
                        html(outfile, "<a href='#"+item.extends+"'>"+item.extends+"</a>", level+1)
                        html(outfile, "</div>", level)
                    
                    html(outfile, "<p><span class='ui medium text'>"+markdown_to_html(item.description)+"</span></p>", level)
                    
                    html(outfile, "<div class='ui message'>", level)
                    html(outfile, "<div class='header'>", level+1)
                    html(outfile, "Constructor syntax", level+2)
                    html(outfile, "</div>", level+1)
                    html(outfile, "<code class='ui large'>"+item.call+"</code>", level+1)
                    html(outfile, "</div>", level)
                    
                    if len(item.params) > 0:
                        #params
                        html(outfile, "<h3 class='ui dividing header'>Constructor parameters</h3>", level)                    
                        html(outfile, "<table class='ui striped table'>", level)
                        html(outfile, "<thead>", level+1)
                        html(outfile, "<tr>", level+2)
                        html(outfile, "<th>Name</th>", level+3)
                        html(outfile, "<th>Type</th>", level+3)
                        html(outfile, "<th>Description</th>", level+3)
                        html(outfile, "<th>Required?</th>", level+3)
                        html(outfile, "</tr>", level+2)
                        html(outfile, "</thead>", level+1)
                        html(outfile, "<tbody>", level+1)
                        for p in item.params:                        
                            html(outfile, "<tr>", level+2)
                            html(outfile, "<td><code>"+p.name+"</code></td>", level+3)
                            html(outfile, "<td><code>"+p.type+"</code></td>", level+3)
                            html(outfile, "<td>"+markdown_to_html(p.description)+"</td>", level+3)
                            html(outfile, "<td>"+("No" if p.optional else "Yes")+"</td>", level+3)
                            html(outfile, "</tr>", level+2)
                        html(outfile, "</tbody>", level+1)
                        html(outfile, "</table>", level)
                    
                    if len(item.methods) > 0:
                        #methods
                        html(outfile, "<h3 class='ui dividing header'>Methods</h3>", level)
                        for m in item.methods:
                            html(outfile, "<div id='"+item.name+"."+m.name+"' class='ui segment'>", level)
                            html(outfile, "<div class='ui left horizontal label teal'>METHOD</div>", level+1)
                            #html(outfile, "<span class='ui medium text'><strong>"+m.name+"</strong></span>&nbsp;&nbsp;<code>"+m.call+"</code>", level+1)
                            html(outfile, "<code>"+m.call+"</code>", level+1)
                            html(outfile, "</div>", level)
                            html(outfile, "<p>"+markdown_to_html(m.description)+"</p>", level)
                            if len(m.params) > 0:
                                html(outfile, "<h4 class='ui dividing header'>Parameters</h4>", level)
                                html(outfile, "<table class='ui striped table'>", level)
                                html(outfile, "<thead>", level+1)
                                html(outfile, "<tr>", level+2)
                                html(outfile, "<th>Name</th>", level+3)
                                html(outfile, "<th>Type</th>", level+3)
                                html(outfile, "<th>Description</th>", level+3)
                                html(outfile, "<th>Required?</th>", level+3)
                                html(outfile, "</tr>", level+2)
                                html(outfile, "</thead>", level+1)
                                html(outfile, "<tbody>", level+1)
                                for p in m.params:                        
                                    html(outfile, "<tr>", level+2)
                                    html(outfile, "<td><code>"+p.name+"</code></td>", level+3)
                                    html(outfile, "<td><code>"+p.type+"</code></td>", level+3)
                                    html(outfile, "<td>"+markdown_to_html(p.description)+"</td>", level+3)
                                    html(outfile, "<td>"+("No" if p.optional else "Yes")+"</td>", level+3)
                                    html(outfile, "</tr>", level+2)
                                html(outfile, "</tbody>", level+1)
                                html(outfile, "</table>", level)
                            
                            if m.returnValue != "" and m.returnValue != None:
                                html(outfile, "<div class='ui'>", level)
                                html(outfile, "<div class='ui right pointing label'>Returns</div>&nbsp;&nbsp;", level+1)
                                html(outfile, "<code>"+m.returnValue.type+"</code>&nbsp;&nbsp;", level+1)
                                html(outfile, "<span>"+markdown_to_html(m.returnValue.description)+"</span>", level+1)
                                html(outfile, "</div>", level)
                    
                    if item.returnValue != "" and item.returnValue != None:
                        html(outfile, "<h3 class='ui dividing header'>Struct return value</h3>", level)                    
                        html(outfile, "<div class='ui'>", level)
                        html(outfile, "<div class='ui right pointing label'>Returns</div>&nbsp;&nbsp;", level+1)
                        html(outfile, "<code>"+item.returnValue.type+"</code>&nbsp;&nbsp;", level+1)
                        html(outfile, "<span>"+markdown_to_html(item.returnValue.description)+"</span>", level+1)
                        html(outfile, "</div>", level)
                
                # functions
                for item in functions:
                    html(outfile, "<div id='"+item.name+"' class='ui raised segment'>", level)
                    html(outfile, "<div class='ui green ribbon label'>FUNCTION</div>", level+1)
                    html(outfile, "<span class='ui large text'>"+item.name+"</span>", level+1)
                    html(outfile, "</div>", level)                
    
                    html(outfile, "<p><span class='ui medium text'>"+markdown_to_html(item.description)+"</span></p>", level)
                    
                    html(outfile, "<div class='ui message'>", level)
                    html(outfile, "<div class='header'>", level+1)
                    html(outfile, "Syntax", level+2)
                    html(outfile, "</div>", level+1)
                    html(outfile, "<code class='ui large'>"+item.call+"</code>", level+1)
                    html(outfile, "</div>", level)
                    
                    if len(item.params) > 0:
                        #params
                        html(outfile, "<h3 class='ui dividing header'>Parameters</h3>", level)                    
                        html(outfile, "<table class='ui striped table'>", level)
                        html(outfile, "<thead>", level+1)
                        html(outfile, "<tr>", level+2)
                        html(outfile, "<th>Name</th>", level+3)
                        html(outfile, "<th>Type</th>", level+3)
                        html(outfile, "<th>Description</th>", level+3)
                        html(outfile, "<th>Required?</th>", level+3)
                        html(outfile, "</tr>", level+2)
                        html(outfile, "</thead>", level+1)
                        html(outfile, "<tbody>", level+1)
                        for p in item.params:                        
                            html(outfile, "<tr>", level+2)
                            html(outfile, "<td><code>"+p.name+"</code></td>", level+3)
                            html(outfile, "<td><code>"+p.type+"</code></td>", level+3)
                            html(outfile, "<td>"+markdown_to_html(p.description)+"</td>", level+3)
                            html(outfile, "<td>"+("No" if p.optional else "Yes")+"</td>", level+3)
                            html(outfile, "</tr>", level+2)
                        html(outfile, "</tbody>", level+1)
                        html(outfile, "</table>", level)
                    
                    if item.returnValue != "" and item.returnValue != None:
                        html(outfile, "<div class='ui'>", level)
                        html(outfile, "<div class='ui right pointing label'>Returns</div>&nbsp;&nbsp;", level+1)
                        html(outfile, "<code>"+item.returnValue.type+"</code>&nbsp;&nbsp;", level+1)
                        html(outfile, "<span>"+markdown_to_html(item.returnValue.description)+"</span>", level+1)
                        html(outfile, "</div>", level)
                
                
                
                
                html(outfile, "</div>", level)
                
                # Bottom
                with open(output_directory+'\\bottom.inc', 'r', encoding = 'utf-8') as bottomfile:
                    for l in bottomfile:
                        outfile.write(l)
        


end_time = datetime.now()
total_time = time.perf_counter() - total_time


print()
print(f"Finished - total time: {total_time} seconds")
if total_structs_documented + total_functions_documented > 0:
    print(f":) Automagically documented {total_structs_documented} structs and {total_functions_documented} functions!")
else:
    print(":( No structs nor functions documented - don't be lazy and add proper GMLDocMaker documentation to your GML!")

print("Ended at "+end_time.strftime("%Y%m%d %H:%M:%S"))