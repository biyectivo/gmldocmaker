﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ProcessingForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProcessingForm))
        Me.htmlBackgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.logTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.logProgressBar = New System.Windows.Forms.ProgressBar()
        Me.LogTextbox = New System.Windows.Forms.RichTextBox()
        Me.logTableLayoutPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'htmlBackgroundWorker
        '
        Me.htmlBackgroundWorker.WorkerReportsProgress = True
        Me.htmlBackgroundWorker.WorkerSupportsCancellation = True
        '
        'logTableLayoutPanel
        '
        Me.logTableLayoutPanel.ColumnCount = 1
        Me.logTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.logTableLayoutPanel.Controls.Add(Me.logProgressBar, 0, 1)
        Me.logTableLayoutPanel.Controls.Add(Me.LogTextbox, 0, 0)
        Me.logTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.logTableLayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me.logTableLayoutPanel.Name = "logTableLayoutPanel"
        Me.logTableLayoutPanel.RowCount = 2
        Me.logTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.logTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.logTableLayoutPanel.Size = New System.Drawing.Size(1157, 519)
        Me.logTableLayoutPanel.TabIndex = 11
        '
        'logProgressBar
        '
        Me.logProgressBar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.logProgressBar.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.logProgressBar.Location = New System.Drawing.Point(3, 482)
        Me.logProgressBar.Name = "logProgressBar"
        Me.logProgressBar.Size = New System.Drawing.Size(1151, 34)
        Me.logProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.logProgressBar.TabIndex = 11
        '
        'LogTextbox
        '
        Me.LogTextbox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LogTextbox.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LogTextbox.Location = New System.Drawing.Point(3, 3)
        Me.LogTextbox.Name = "LogTextbox"
        Me.LogTextbox.ReadOnly = True
        Me.LogTextbox.Size = New System.Drawing.Size(1151, 473)
        Me.LogTextbox.TabIndex = 12
        Me.LogTextbox.Text = ""
        '
        'ProcessingForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1157, 519)
        Me.Controls.Add(Me.logTableLayoutPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ProcessingForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "GMLDocMaker - Generating documentation..."
        Me.logTableLayoutPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents htmlBackgroundWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents logTableLayoutPanel As TableLayoutPanel
    Friend WithEvents logProgressBar As ProgressBar
    Friend WithEvents LogTextbox As RichTextBox
End Class
