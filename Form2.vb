﻿Imports System.ComponentModel
Imports System.Diagnostics.Eventing
Imports System.IO
Imports System.Net.Http

Public Class ProcessingForm

    Dim std_out_txt As String
    Dim std_err_txt As String

    Private _form1 As MainForm


    Public Sub AssignForm(frm As Form)
        _form1 = frm
    End Sub

    Private Sub ProcessingForm_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        logTableLayoutPanel.Width = Me.Width - 40
        logTableLayoutPanel.Height = Me.Height - 60
    End Sub

    Private Sub ProcessingForm_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        _form1.Show()
    End Sub

    Private Sub htmlBackgroundWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles htmlBackgroundWorker.DoWork

        Dim path As String = FileSystem.CurDir

        ' Copy image
        If _form1.imageText.Text <> "" Then
            FileCopy(_form1.imageText.Text, path + "\img\image.png")
        Else
            FileCopy(path + "\include\default.png", path + "\img\image.png")
        End If

        ' Create file with list of GML files to process
        FileOpen(1, path + "\file_list.txt", OpenMode.Output)
        For i = 0 To _form1.CheckedListBox1.Items.Count - 1
            If _form1.CheckedListBox1.GetItemChecked(i) Then
                FileSystem.PrintLine(1, Replace(_form1.CheckedListBox1.Items(i), Chr(34), ""))
            End If
        Next
        FileClose(1)

        ' Generate command
        Dim arguments As String = " " + Chr(34) + path + "\file_list.txt" + Chr(34) + " " + Chr(34) + _form1.outDirectoryTextbox.Text + Chr(34) + " " + Chr(34) + _form1.pageTitleTextbox.Text + Chr(34) + " " + Chr(34) + _form1.versionTextbox.Text + Chr(34)
        Dim executable As String = path + "\python\ParseFiles.exe"

        ' Create BAT for CLI
        FileOpen(1, path + "\cli.bat", OpenMode.Output)
        FileSystem.PrintLine(1, executable + arguments)
        FileClose(1)

        ' Set start information.
        Dim start_info As New ProcessStartInfo(executable)
        start_info.UseShellExecute = False
        start_info.CreateNoWindow = True
        start_info.RedirectStandardOutput = True
        start_info.RedirectStandardError = True
        start_info.Arguments = arguments

        ' Make the process and set its start information.
        Dim proc As New Process()
        proc.StartInfo = start_info

        ' Start the process.
        proc.Start()

        ' Attach to stdout and stderr.
        Dim std_out As StreamReader = proc.StandardOutput()
        Dim std_err As StreamReader = proc.StandardError()

        ' Display the results.
        std_out_txt = std_out.ReadToEnd()
        std_err_txt = std_err.ReadToEnd()

        ' Clean up.
        std_out.Close()
        std_err.Close()
        proc.Close()

        htmlBackgroundWorker.ReportProgress(100, "Complete")
    End Sub


    Private Sub htmlBackgroundWorker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs)
        MsgBox("Finished")
    End Sub

    Private Sub htmlBackgroundWorker_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles htmlBackgroundWorker.ProgressChanged
        LogTextbox.AppendText(std_out_txt)
        If (std_err_txt <> "") Then
            LogTextbox.AppendText("ERRORS:")
            LogTextbox.AppendText(std_err_txt)
        End If
        logProgressBar.Value = e.ProgressPercentage

        If (e.ProgressPercentage = 100) Then
            Me.Text = "GMLDocMaker - Finished (you can close this window now)"
        End If
    End Sub
End Class