﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.inDirectoryBrowserDialog = New System.Windows.Forms.FolderBrowserDialog()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.inDirectoryBrowseButton = New System.Windows.Forms.Button()
        Me.inDirectoryTextbox = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.uncheckAllButton = New System.Windows.Forms.Button()
        Me.checkAllButton = New System.Windows.Forms.Button()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.versionTextbox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.imageText = New System.Windows.Forms.TextBox()
        Me.pageTitleTextbox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.outDirectoryTextbox = New System.Windows.Forms.TextBox()
        Me.outDirectoryBrowseButton = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.outDirectoryBrowserDialog = New System.Windows.Forms.FolderBrowserDialog()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.makeDocumentationButton = New System.Windows.Forms.Button()
        Me.selectImageDialog = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'inDirectoryBrowserDialog
        '
        Me.inDirectoryBrowserDialog.ShowNewFolderButton = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 446)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 13)
        Me.Label3.TabIndex = 6
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.inDirectoryBrowseButton)
        Me.GroupBox1.Controls.Add(Me.inDirectoryTextbox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1005, 60)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "1. Select (or paste) your Gamemaker project or script folder:"
        '
        'inDirectoryBrowseButton
        '
        Me.inDirectoryBrowseButton.Location = New System.Drawing.Point(903, 21)
        Me.inDirectoryBrowseButton.Name = "inDirectoryBrowseButton"
        Me.inDirectoryBrowseButton.Size = New System.Drawing.Size(85, 20)
        Me.inDirectoryBrowseButton.TabIndex = 2
        Me.inDirectoryBrowseButton.Text = "Browse..."
        Me.inDirectoryBrowseButton.UseVisualStyleBackColor = True
        '
        'inDirectoryTextbox
        '
        Me.inDirectoryTextbox.Location = New System.Drawing.Point(21, 22)
        Me.inDirectoryTextbox.Name = "inDirectoryTextbox"
        Me.inDirectoryTextbox.Size = New System.Drawing.Size(861, 20)
        Me.inDirectoryTextbox.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.uncheckAllButton)
        Me.GroupBox2.Controls.Add(Me.checkAllButton)
        Me.GroupBox2.Controls.Add(Me.CheckedListBox1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 88)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1005, 304)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "2. Check the scripts for which you want to generate documentation:"
        '
        'uncheckAllButton
        '
        Me.uncheckAllButton.Location = New System.Drawing.Point(102, 265)
        Me.uncheckAllButton.Name = "uncheckAllButton"
        Me.uncheckAllButton.Size = New System.Drawing.Size(75, 23)
        Me.uncheckAllButton.TabIndex = 5
        Me.uncheckAllButton.Text = "Uncheck all"
        Me.uncheckAllButton.UseVisualStyleBackColor = True
        '
        'checkAllButton
        '
        Me.checkAllButton.Location = New System.Drawing.Point(21, 266)
        Me.checkAllButton.Name = "checkAllButton"
        Me.checkAllButton.Size = New System.Drawing.Size(75, 23)
        Me.checkAllButton.TabIndex = 4
        Me.checkAllButton.Text = "Check all"
        Me.checkAllButton.UseVisualStyleBackColor = True
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.HorizontalScrollbar = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(21, 30)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.ScrollAlwaysVisible = True
        Me.CheckedListBox1.Size = New System.Drawing.Size(967, 214)
        Me.CheckedListBox1.TabIndex = 3
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.versionTextbox)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.Button1)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.imageText)
        Me.GroupBox4.Controls.Add(Me.pageTitleTextbox)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 486)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1005, 132)
        Me.GroupBox4.TabIndex = 17
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "4. Set your options:"
        '
        'versionTextbox
        '
        Me.versionTextbox.Location = New System.Drawing.Point(117, 55)
        Me.versionTextbox.Name = "versionTextbox"
        Me.versionTextbox.Size = New System.Drawing.Size(61, 20)
        Me.versionTextbox.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Project version"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(441, 81)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(85, 20)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Browse..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(19, 84)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Project logo (PNG)"
        '
        'imageText
        '
        Me.imageText.Location = New System.Drawing.Point(117, 82)
        Me.imageText.Name = "imageText"
        Me.imageText.Size = New System.Drawing.Size(319, 20)
        Me.imageText.TabIndex = 10
        '
        'pageTitleTextbox
        '
        Me.pageTitleTextbox.Location = New System.Drawing.Point(117, 28)
        Me.pageTitleTextbox.Name = "pageTitleTextbox"
        Me.pageTitleTextbox.Size = New System.Drawing.Size(195, 20)
        Me.pageTitleTextbox.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Project name"
        '
        'outDirectoryTextbox
        '
        Me.outDirectoryTextbox.Location = New System.Drawing.Point(21, 23)
        Me.outDirectoryTextbox.Name = "outDirectoryTextbox"
        Me.outDirectoryTextbox.Size = New System.Drawing.Size(861, 20)
        Me.outDirectoryTextbox.TabIndex = 6
        '
        'outDirectoryBrowseButton
        '
        Me.outDirectoryBrowseButton.Location = New System.Drawing.Point(903, 23)
        Me.outDirectoryBrowseButton.Name = "outDirectoryBrowseButton"
        Me.outDirectoryBrowseButton.Size = New System.Drawing.Size(85, 20)
        Me.outDirectoryBrowseButton.TabIndex = 7
        Me.outDirectoryBrowseButton.Text = "Browse..."
        Me.outDirectoryBrowseButton.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.outDirectoryBrowseButton)
        Me.GroupBox3.Controls.Add(Me.outDirectoryTextbox)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 408)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1005, 63)
        Me.GroupBox3.TabIndex = 16
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "3. Select (or paste) the output folder for the generated HTML documentation:"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.makeDocumentationButton)
        Me.GroupBox5.Location = New System.Drawing.Point(12, 635)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(1005, 82)
        Me.GroupBox5.TabIndex = 18
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "5. Generate your docs!"
        '
        'makeDocumentationButton
        '
        Me.makeDocumentationButton.Location = New System.Drawing.Point(20, 29)
        Me.makeDocumentationButton.Name = "makeDocumentationButton"
        Me.makeDocumentationButton.Size = New System.Drawing.Size(968, 38)
        Me.makeDocumentationButton.TabIndex = 12
        Me.makeDocumentationButton.Text = "Generate HTML5 Documentation"
        Me.makeDocumentationButton.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1029, 729)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "GMLDocMaker vX.X by manta ray"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents inDirectoryBrowserDialog As FolderBrowserDialog
    Friend WithEvents Label3 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents inDirectoryBrowseButton As Button
    Friend WithEvents inDirectoryTextbox As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents CheckedListBox1 As CheckedListBox
    Friend WithEvents outDirectoryTextbox As TextBox
    Friend WithEvents outDirectoryBrowseButton As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents outDirectoryBrowserDialog As FolderBrowserDialog
    Friend WithEvents pageTitleTextbox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents uncheckAllButton As Button
    Friend WithEvents checkAllButton As Button
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents makeDocumentationButton As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents imageText As TextBox
    Friend WithEvents selectImageDialog As OpenFileDialog
    Friend WithEvents versionTextbox As TextBox
    Friend WithEvents Label5 As Label
End Class
